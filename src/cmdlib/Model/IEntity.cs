﻿
namespace Anycmd.Model
{

    /// <summary>
    /// 标记接口。表示该类型是实体，实体一般在数据库中对应有持久表或视图。
    /// </summary>
    public interface IEntity<TEntityId>
    {
        TEntityId Id { get; }
    }
}
